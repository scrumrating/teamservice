﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using TeamService.Consumers.Infrastructure;
using TeamService.Core.DTO;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Services;
using TeamService.Data.Context;
using TeamService.Data.Repositories;

namespace TeamService.Instance
{
    /// <summary>
    /// Инициализация приложения.
    /// </summary>
    public class Startup
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Startup"/>.
        /// </summary>
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            this.configuration = configuration;
            ConfigureDataStorage();
        }

        /// <summary>
        /// Конфигурация приложения.
        /// </summary>
        /// <param name="application">Приложение.</param>
        public void Configure(IApplicationBuilder application)
        {
            application.UseCors("AllowAll");
            application.UseMvc();
            application.UseSwaggerDocumentation(this.configuration);
        }

        /// <summary>
        /// Конфигурация сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(this.configuration)
                .CreateLogger();

            Log.Information("Начинается регистрация политик CORS.");

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            Log.Information("Регистрация политик CORS успешно завершена.");

            Log.Information("Начинается регистрация сервисов.");

            Log.Information("Начинается регистрация Swagger генератора.");

            services.AddSwaggerDocumentation(this.configuration);

            Log.Information("Регистрация Swagger генератора завершена.");

            Log.Information("Регистрация сервиса Team");
            services.AddScoped<DbContext, TeamContext>();
            services.AddDbContext<TeamContext>(option =>
                option.UseNpgsql(this.configuration.GetConnectionString("Teams")));

            services.AddScoped<IServiceAsync<Team>, TeamServiceAsync>();
            services.AddScoped<IServiceAsync<CompositionTeam>, CompositionTeamServiceAsync>();
            services.AddScoped<IRepositoryAsync<TeamDto>, EFRepository<TeamDto>>();
            services.AddScoped<IRepositoryAsync<CompositionTeamDto>, EFRepository<CompositionTeamDto>>();

            //services.AddCore();

            //services.AddData();

            services.AddConsumers();

            Log.Information("Сервис Student зарегистрирован.");

            Log.Information("Регистрация сервисов успешно завершена.");

            Log.Information("Начинается регистрация шины.");

            services.BusRegistration(this.configuration);

            Log.Information("Регистрация шины успешно завершена.");

            Log.Information("Начинается регистрация фильтра авторизации.");

            Log.Information("Начинается регистрация сервисов MVC.");

            services.AddMvc();

            Log.Information("Регистрация сервисов MVC успешно завершена.");
        }

        /// <summary>
        /// Конфигурирует базу данных.
        /// </summary>
        public void ConfigureDataStorage()
        {
            Log.Information("Начинается создание/обновление базы данных.");

            var contextOptions = new DbContextOptionsBuilder<TeamContext>();
            contextOptions.UseNpgsql(this.configuration.GetConnectionString("Teams"));

            using (var context = new TeamContext(contextOptions.Options))
            {
                context.Database.EnsureCreated();
            }

            Log.Information("Создание/обновление базы данных завершено.");
        }
    }
}