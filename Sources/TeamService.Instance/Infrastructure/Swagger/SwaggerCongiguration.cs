﻿namespace TeamService.Instance
{
    /// <summary>
    /// Конфигурация swagger.
    /// </summary>
    public class SwaggerCongiguration
    {
        /// <summary>
        /// Путь к UI swagger
        /// </summary>
        public string Uri { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Описание странички.
        /// </summary>
        public string Description { get; set; }
    }
}