﻿using System;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TeamService.Consumers;
using TeamService.Consumers.AddStudentsToTeam;
using TeamService.Consumers.AddStudentToTeam;
using TeamService.Consumers.Create;
using TeamService.Consumers.Delete;
using TeamService.Consumers.GetAll;
using TeamService.Consumers.GetAllPagination;
using TeamService.Consumers.GetAllStudentsByTeamId;
using TeamService.Consumers.GetTeamById;
using TeamService.Consumers.GetTeamByName;
using TeamService.Consumers.Healthchecks;
using TeamService.Consumers.RemoveStudentFromTeam;
using TeamService.Consumers.Update;

namespace TeamService.Instance
{
    /// <summary>
    /// Регистрация consumers в приложении.
    /// </summary>
    internal static class MessageBrokerRegistration
    {
        /// <summary>
        /// Регистрирует броекр сообщений.
        /// </summary>
        /// <param name="services">Список сервисов.</param>
        /// <returns>Изменённый список сервисов.</returns>
        public static IServiceCollection BusRegistration(this IServiceCollection services, IConfiguration configuration)
        {
          // Регистрация потребителей сообщений
            services.AddMassTransit(x =>
            {
                x.AddConsumer<HealthcheckConsumer>();
                x.AddConsumer<AddStudentsToTeamConsumer>();
                x.AddConsumer<AddStudentToTeamConsumer>();
                x.AddConsumer<GetAllStudentsByTeamIdConsumer>();
                x.AddConsumer<CreateConsumer>();
                x.AddConsumer<DeleteConsumer>();
                x.AddConsumer<GetAllConsumer>();
                x.AddConsumer<GetAllPaginationConsumer>();
                x.AddConsumer<GetTeamByIdConsumer>();
                x.AddConsumer<GetTeamByNameConsumer>();
                x.AddConsumer<RemoveStudentFromTeamConsumer>();
                x.AddConsumer<UpdateConsumer>();
            });

            var busConfiguration = configuration.GetSection("Bus").Get<BusConfiguration>();
            
            // Регистрация шины.
            // Подробнее про регистрацию шины можно почитать здесь: https://masstransit-project.com/usage/
            services.AddSingleton(serviceProvider => Bus.Factory.CreateUsingRabbitMq(configure =>
            {
                
                // Конфигурация подключения к шине, включающая в себя указание адреса и учетных данных.
                configure.Host(new Uri(busConfiguration.ConnectionString), host =>
                {
                    host.Username(busConfiguration.Username);
                    host.Password(busConfiguration.Password);

                    // Подтверждение получения гарантирует доставку сообщений за счет ухудшения производительности.
                    host.PublisherConfirmation = busConfiguration.PublisherConfirmation;
                });
                
                // Добавление Serilog для журналирования внутренностей MassTransit.
                configure.UseSerilog();

                // Регистрация очередей и их связи с потребителями сообщений.
                // В качестве метки сообщения используется полное имя класса сообщения, которое потребляет потребитель.
                configure.ReceiveEndpoint(typeof(HealthcheckCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<HealthcheckConsumer>(serviceProvider);
                    EndpointConvention.Map<HealthcheckCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(AddStudentsToTeamCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<AddStudentsToTeamConsumer>(serviceProvider);
                    EndpointConvention.Map<AddStudentsToTeamCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(AddStudentToTeamCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<AddStudentToTeamConsumer>(serviceProvider);
                    EndpointConvention.Map<AddStudentToTeamCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetAllStudentsByTeamIdCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetAllStudentsByTeamIdConsumer>(serviceProvider);
                    EndpointConvention.Map<GetAllStudentsByTeamIdCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(CreateCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<CreateConsumer>(serviceProvider);
                    EndpointConvention.Map<CreateCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(DeleteCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<DeleteConsumer>(serviceProvider);
                    EndpointConvention.Map<DeleteCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetAllCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetAllConsumer>(serviceProvider);
                    EndpointConvention.Map<GetAllCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetAllPaginationCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetAllPaginationConsumer>(serviceProvider);
                    EndpointConvention.Map<GetAllPaginationCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetTeamByIdCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetTeamByIdConsumer>(serviceProvider);
                    EndpointConvention.Map<GetTeamByIdCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetTeamByNameCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetTeamByNameConsumer>(serviceProvider);
                    EndpointConvention.Map<GetTeamByNameCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(RemoveStudentFromTeamCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<RemoveStudentFromTeamConsumer>(serviceProvider);
                    EndpointConvention.Map<RemoveStudentFromTeamCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(UpdateCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<UpdateConsumer>(serviceProvider);
                    EndpointConvention.Map<UpdateCommand>(endpoint.InputAddress);
                });
            }));

            // Регистрация сервисов MassTransit.
            services.AddSingleton<IPublishEndpoint>(
                serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<ISendEndpointProvider>(serviceProvider =>
                serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<IBus>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());

            // Регистрация клиентов для запроса данных от потребителей сообщений из api.
            // Каждый клиент зарегистрирован таким образом, что бы в рамках каждого запроса к api существовал свой клиент.
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<HealthcheckCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<AddStudentsToTeamCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<AddStudentToTeamCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllStudentsByTeamIdCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<CreateCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<DeleteCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllPaginationCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetTeamByIdCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetTeamByNameCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<RemoveStudentFromTeamCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<UpdateCommand>());

            // Регистрация сервиса шины MassTransit.
            services.AddSingleton<IHostedService, BusService>();
            return services;
        }
    }
}