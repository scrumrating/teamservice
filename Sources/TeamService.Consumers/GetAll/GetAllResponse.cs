﻿using System.Collections;
using TeamService.Core;
using System.Collections.Generic;
using TeamService.Core.Entities;

namespace TeamService.Consumers
{
    /// <summary>
    /// Ответ на команду получения списка команд.
    /// </summary>
    public class GetAllResponse
    {
        /// <summary>
        /// Получает или задает список команд.
        /// </summary>
        public IEnumerable<Team> Teams { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}