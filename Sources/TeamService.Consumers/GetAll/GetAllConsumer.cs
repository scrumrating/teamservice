﻿using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.GetAll
{
    /// <summary>
    /// Обработчик сообщения получения списка команд.
    /// </summary>
    public class GetAllConsumer : IConsumer<GetAllCommand>
    {
        private readonly IServiceAsync<Team> service;
        private readonly ILogger<GetAllConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetAllConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetAllConsumer(IServiceAsync<Team> service, ILogger<GetAllConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetAllCommand> context)
        {
            this.logger.LogInformation("Выполняется обработка сообщения получения всех команд.");

            var teams = await this.service.GetAsync();

            if (teams != null)
            {
                await context.RespondAsync(new GetAllResponse { Teams = teams, Result = ResponseResult.Success });
            }

            await context.RespondAsync(new GetAllResponse { Result = ResponseResult.NotSuccess });
        }
    }
}