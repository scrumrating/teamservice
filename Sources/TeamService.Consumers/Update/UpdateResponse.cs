﻿namespace TeamService.Consumers.Update
{
    /// <summary>
    /// Ответ на команду обновления данных команды.
    /// </summary>
    public class UpdateResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}