﻿using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.Update
{
    /// <summary>
    /// Обработчик сообщения обновления данных о команде.
    /// </summary>
    public class UpdateConsumer : IConsumer<UpdateCommand>
    {
        private readonly IServiceAsync<Team> service;
        private readonly ILogger<UpdateConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UpdateConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public UpdateConsumer(IServiceAsync<Team> service, ILogger<UpdateConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<UpdateCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения обновления данных команды '{context.Message.Team}'.");

            await this.service.UpdateAsync(context.Message.Team);
            await context.RespondAsync(new UpdateResponse { Result = ResponseResult.Success });
        }
    }
}