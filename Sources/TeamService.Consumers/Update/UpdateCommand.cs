﻿using TeamService.Core.Entities;

namespace TeamService.Consumers.Update
{
    /// <summary>
    /// Команда для обновления данных команды.
    /// </summary>
    public class UpdateCommand
    {
        /// <summary>
        /// Получает или задает данные команды.
        /// </summary>
        public Team Team { get; set; }
    }
}