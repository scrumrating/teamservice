﻿using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.Create
{
    /// <summary>
    /// Обработчик сообщения создания команды.
    /// </summary>
    public class CreateConsumer : IConsumer<CreateCommand>
    {
        private readonly IServiceAsync<Team> service;
        private readonly ILogger<CreateConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CreateConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public CreateConsumer(IServiceAsync<Team> service, ILogger<CreateConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<CreateCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения создания команды '{context.Message.Team}'");

            var result = await this.service.CreateAsync(context.Message.Team);

            if (result)
            {
                await context.RespondAsync(new CreateResponse() { Result = ResponseResult.Success });
            }

            await context.RespondAsync(new CreateResponse { Result = ResponseResult.NotSuccess });
        }
    }
}