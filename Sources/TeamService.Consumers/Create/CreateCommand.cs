﻿using TeamService.Core.Entities;

namespace TeamService.Consumers.Create
{
    /// <summary>
    /// Команда для создания команды.
    /// </summary>
    public class CreateCommand
    {
        /// <summary>
        /// Получает или задает данные команды.
        /// </summary>
        public Team Team { get; set; }
    }
}