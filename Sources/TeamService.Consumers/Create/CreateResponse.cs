﻿namespace TeamService.Consumers.Create
{
    /// <summary>
    /// Ответ на команду создания команды.
    /// </summary>
    public class CreateResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}