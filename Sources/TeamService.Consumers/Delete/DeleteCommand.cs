﻿using TeamService.Core.Entities;

namespace TeamService.Consumers.Delete
{
    /// <summary>
    /// Команда для удаления данных о команде.
    /// </summary>
    public class DeleteCommand
    {
        /// <summary>
        /// Получает или задает объект коамнды.
        /// </summary>
        public int Id { get; set; }
    }
}