﻿namespace TeamService.Consumers.Delete
{
    /// <summary>
    /// Ответ на команду удаления данных о команде.
    /// </summary>
    public class DeleteResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}