﻿using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.Delete
{
    /// <summary>
    /// Обработчик сообщения получения удаления данных о команде.
    /// </summary>
    public class DeleteConsumer : IConsumer<DeleteCommand>
    {
        private readonly IServiceAsync<Team> service;
        private readonly ILogger<DeleteConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="DeleteConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public DeleteConsumer(IServiceAsync<Team> service, ILogger<DeleteConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<DeleteCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения удаления данных с Id '{context.Message.Id}'");

            await this.service.RemoveAsync(context.Message.Id);
            await context.RespondAsync(new DeleteResponse() { Result = ResponseResult.Success });
        }
    }
}