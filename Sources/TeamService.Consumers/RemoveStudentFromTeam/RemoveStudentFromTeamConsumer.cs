﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.RemoveStudentFromTeam
{
    /// <summary>
    /// Обработчик сообщения удаление студента из команды.
    /// </summary>
    public class RemoveStudentFromTeamConsumer : IConsumer<RemoveStudentFromTeamCommand>
    {
        private readonly IServiceAsync<Team> teamService;
        private readonly IServiceAsync<CompositionTeam> compositionTeamService;
        private readonly ILogger<RemoveStudentFromTeamConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="RemoveStudentFromTeamConsumer"/>.
        /// </summary>
        /// <param name="teamService">Сервисный объект команды.</param>
        /// <param name="compositionTeamService">Сервисный объект состава команды.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public RemoveStudentFromTeamConsumer(IServiceAsync<Team> teamService, IServiceAsync<CompositionTeam> compositionTeamService, ILogger<RemoveStudentFromTeamConsumer> logger)
        {
            this.teamService = teamService;
            this.compositionTeamService = compositionTeamService;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<RemoveStudentFromTeamCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения удаление студента из коанды '{context.Message.TeamId}'");

            var compositionTeam = (await this.compositionTeamService.GetAsync(item =>
                item.StudentId.Equals(context.Message.StudentId) &&
                item.TeamId.Equals(context.Message.TeamId))).FirstOrDefault();
            if (compositionTeam != null)
            {
                await this.compositionTeamService.RemoveAsync(compositionTeam.Id);
                await context.RespondAsync(new RemoveStudentFromTeamResponse() { Result = ResponseResult.Success });
            }

            await context.RespondAsync(new RemoveStudentFromTeamResponse { Result = ResponseResult.NotSuccess });
        }
    }
}