﻿using TeamService.Core.Entities;

namespace TeamService.Consumers.RemoveStudentFromTeam
{
    /// <summary>
    /// Команда для удаления данных студента.
    /// </summary>
    public class RemoveStudentFromTeamCommand
    {
        /// <summary>
        /// Получает или задает идентификатор студента.
        /// </summary>
        public int StudentId { get; set; }
        
        /// <summary>
        /// Получает или задает данные команды.
        /// </summary>
        public int TeamId { get; set; }
    }
}