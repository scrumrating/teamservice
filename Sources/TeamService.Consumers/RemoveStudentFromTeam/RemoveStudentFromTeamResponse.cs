﻿namespace TeamService.Consumers.RemoveStudentFromTeam
{
    /// <summary>
    /// Ответ на команду удаления данных из команды.
    /// </summary>
    public class RemoveStudentFromTeamResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}