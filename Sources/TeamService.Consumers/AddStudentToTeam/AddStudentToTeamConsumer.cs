﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.AddStudentToTeam
{
    /// <summary>
    /// Обработчик сообщения добавление студента в команду.
    /// </summary>
    public class AddStudentToTeamConsumer : IConsumer<AddStudentToTeamCommand>
    {
        private readonly IServiceAsync<Team> teamService;
        private readonly IServiceAsync<CompositionTeam> compositionTeamService;
        private readonly ILogger<AddStudentToTeamConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AddStudentToTeamConsumer"/>.
        /// </summary>
        /// <param name="teamService">Сервисный объект команды.</param>
        /// <param name="compositionTeamService">Сервисный объект состава команды.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public AddStudentToTeamConsumer(IServiceAsync<Team> teamService, IServiceAsync<CompositionTeam> compositionTeamService, ILogger<AddStudentToTeamConsumer> logger)
                 {
            this.teamService = teamService;
            this.compositionTeamService = compositionTeamService;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<AddStudentToTeamCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения регистрация студента в команде '{context.Message.TeamId}'");

            var team = (await this.teamService.GetAsync(t => t.Id == (context.Message.TeamId))).FirstOrDefault();
            if (team != null)
            {
                await this.compositionTeamService.CreateAsync(new CompositionTeam(team.Id, context.Message.StudentId));
                await context.RespondAsync(new AddStudentToTeamResponse() { Result = ResponseResult.Success });
            }

            await context.RespondAsync(new AddStudentToTeamResponse { Result = ResponseResult.NotSuccess });
        }
    }
}