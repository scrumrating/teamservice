﻿namespace TeamService.Consumers.AddStudentToTeam
{
    /// <summary>
    /// Ответ на команду добавление студента в команду.
    /// </summary>
    public class AddStudentToTeamResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}