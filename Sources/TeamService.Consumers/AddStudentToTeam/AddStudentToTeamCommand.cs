﻿using TeamService.Core.Entities;

namespace TeamService.Consumers.AddStudentToTeam
{
    /// <summary>
    /// Команда для добавляния студента в команду.
    /// </summary>
    public class AddStudentToTeamCommand
    {
        /// <summary>
        /// Получает или задает id студента.
        /// </summary>
        public int StudentId { get; set; }
        
        /// <summary>
        /// Получает или задает данные команды.
        /// </summary>
        public int TeamId { get; set; }
    }
}