﻿using System.Collections;
using System.Collections.Generic;
using TeamService.Core.Entities;

namespace TeamService.Consumers.GetAllPagination
{
    /// <summary>
    /// Ответ на команду получения списка команд.
    /// </summary>
    public class GetAllPaginationResponse
    {
        /// <summary>
        /// Получает или задает список команд.
        /// </summary>
        public IEnumerable<Team> Teams { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}