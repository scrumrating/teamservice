﻿namespace TeamService.Consumers.GetAllPagination
{
    /// <summary>
    /// Команда для получения списка команд.
    /// </summary>
    public class GetAllPaginationCommand
    {
        /// <summary>
        /// Получает или задает размер страницы.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Получает или задает номер страницы.
        /// </summary>
        public int PageNumber { get; set; }
    }
}