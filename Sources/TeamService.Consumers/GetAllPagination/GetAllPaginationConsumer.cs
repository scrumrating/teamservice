﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.GetAllPagination
{
    /// <summary>
    /// Обработчик сообщения получения списка команд.
    /// </summary>
    public class GetAllPaginationConsumer : IConsumer<GetAllPaginationCommand>
    {
        private readonly IServiceAsync<Team> service;
        private readonly ILogger<GetAllPaginationConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetAllPaginationConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetAllPaginationConsumer(IServiceAsync<Team> service, ILogger<GetAllPaginationConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetAllPaginationCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения получения всех команд с количеством записей: {context.Message.PageSize}, номером страницы: {context.Message.PageNumber}.");

            var teams = await this.service.GetAsync(context.Message.PageSize, context.Message.PageNumber);

            if (teams != null)
            {
                await context.RespondAsync(new GetAllPaginationResponse { Teams = teams, Result = ResponseResult.Success });
            }

            await context.RespondAsync(new GetAllPaginationResponse { Result = ResponseResult.NotSuccess });
        }
    }
}