﻿using TeamService.Core.Entities;

namespace TeamService.Consumers.GetTeamByName
{
    /// <summary>
    /// Ответ на команду поиска информации о команде по названиею.
    /// </summary>
    public class GetTeamByNameResponse
    {
        /// <summary>
        /// Получает или задает команду.
        /// </summary>
        public Team Team { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}