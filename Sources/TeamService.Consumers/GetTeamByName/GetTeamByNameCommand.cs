﻿namespace TeamService.Consumers.GetTeamByName
{
    /// <summary>
    /// Команда поиска команды по названию.
    /// </summary>
    public class GetTeamByNameCommand
    {
        /// <summary>
        /// Получает или задает название команды.
        /// </summary>
        public string Name { get; set; }
    }
}