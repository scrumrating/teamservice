﻿using MassTransit;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.GetTeamByName
{
    /// <summary>
    /// Обработчик сообщения получения информации о команде по названиею.
    /// </summary>
    public class GetTeamByNameConsumer : IConsumer<GetTeamByNameCommand>
    {
        private readonly IServiceAsync<Team> service;
        private readonly ILogger<GetTeamByNameConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetTeamByNameConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetTeamByNameConsumer(IServiceAsync<Team> service, ILogger<GetTeamByNameConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetTeamByNameCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения поиска команды с названием '{context.Message.Name}'");

            var team = (await this.service.GetAsync(t => t.Equals(context.Message.Name))).FirstOrDefault();

            if (team != null)
            {
                await context.RespondAsync(new GetTeamByNameResponse() { Team = team, Result = ResponseResult.Success });
            }

            await context.RespondAsync(new GetTeamByNameResponse { Result = ResponseResult.NotSuccess });
        }
    }
}