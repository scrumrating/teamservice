﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.GetTeamById
{
    /// <summary>
    /// Обработчик сообщения получения информации о команде по Id.
    /// </summary>
    public class GetTeamByIdConsumer : IConsumer<GetTeamByIdCommand>
    {
        private readonly IServiceAsync<Team> service;
        private readonly ILogger<GetTeamByIdConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetTeamByIdConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetTeamByIdConsumer(IServiceAsync<Team> service, ILogger<GetTeamByIdConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetTeamByIdCommand> context)
        {
            this.logger.LogInformation("Выполняется обработка сообщения получения данных по Id.");

            var team = await this.service.FindByIdAsync(context.Message.Id);

            if (team != null)
            {
                await context.RespondAsync(new GetTeamByIdResponse { Team = team, Result = ResponseResult.Success });
            }

            await context.RespondAsync(new GetTeamByIdResponse { Result = ResponseResult.NotSuccess });
        }
    }
}