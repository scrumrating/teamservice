﻿using TeamService.Core.Entities;

namespace TeamService.Consumers.GetTeamById
{
    /// <summary>
    /// Ответ на команду получения данных о команде по id.
    /// </summary>
    public class GetTeamByIdResponse
    {
        /// <summary>
        /// Получает или задаёт данные по команде.
        /// </summary>
        public Team Team { get; set; }
        
        /// <summary>
        /// Получает или задаёт результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}