﻿namespace TeamService.Consumers.GetTeamById
{
    /// <summary>
    /// Команда для получение данных о команде по id.
    /// </summary>
    public class GetTeamByIdCommand
    {
        /// <summary>
        /// Получает или задает идентификатор команды.
        /// </summary>
        public int Id { get; set; }
    }
}