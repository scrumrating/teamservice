﻿namespace TeamService.Consumers.AddStudentsToTeam
{
    /// <summary>
    /// Ответ на команду регистрации студентов.
    /// </summary>
    public class AddStudentsToTeamResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}