﻿using System.Collections;
using System.Collections.Generic;
using TeamService.Core.Entities;

namespace TeamService.Consumers.AddStudentsToTeam
{
    /// <summary>
    /// Команда для добавляния студентов в команду.
    /// </summary>
    public class AddStudentsToTeamCommand
    {
        /// <summary>
        /// Получает или задает id студента.
        /// </summary>
        public List<int> StudentsId { get; set; }

        /// <summary>
        /// Получает или задает данные команды.
        /// </summary>
        public Team Team { get; set; }
    }
}