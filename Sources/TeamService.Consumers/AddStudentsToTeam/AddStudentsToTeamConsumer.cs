﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.AddStudentsToTeam
{
    /// <summary>
    /// Обработчик сообщения добавление студента в команду.
    /// </summary>
    public class AddStudentsToTeamConsumer : IConsumer<AddStudentsToTeamCommand>
    {
        private readonly IServiceAsync<Team> teamService;
        private readonly IServiceAsync<CompositionTeam> compositionTeamService;
        private readonly ILogger<AddStudentsToTeamConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AddStudentsToTeamConsumer"/>.
        /// </summary>
        /// <param name="teamService">Сервисный объект команды.</param>
        /// <param name="compositionTeamService">Сервисный объект состава команды.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public AddStudentsToTeamConsumer(IServiceAsync<Team> teamService, IServiceAsync<CompositionTeam> compositionTeamService, ILogger<AddStudentsToTeamConsumer> logger)
        {
            this.teamService = teamService;
            this.compositionTeamService = compositionTeamService;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<AddStudentsToTeamCommand> context)
        {
            this.logger.LogInformation(
                $"Выполняется обработка сообщения регистрация студентов в команде '{context.Message.Team}'");

            var team = (await this.teamService.GetAsync(t => t.Equals(context.Message.Team))).FirstOrDefault();
            if (team != null)
            {
                foreach (var item in context.Message.StudentsId)
                {
                    await this.compositionTeamService.CreateAsync(new CompositionTeam(team.Id, item));
                }

                await context.RespondAsync(new AddStudentsToTeamResponse() { Result = ResponseResult.Success });
            }

            await context.RespondAsync(new AddStudentsToTeamResponse { Result = ResponseResult.NotSuccess });
        }
    }
}