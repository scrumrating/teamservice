﻿using TeamService.Core.Entities;

namespace TeamService.Consumers.GetAllStudentsByTeamId
{
    /// <summary>
    /// Команда для получения студентов в команде..
    /// </summary>
    public class GetAllStudentsByTeamIdCommand
    {
        /// <summary>
        /// Получает или задает данные команды.
        /// </summary>
        public int Id { get; set; }
    }
}