﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;
using TeamService.Core.Structures;

namespace TeamService.Consumers.GetAllStudentsByTeamId
{
    /// <summary>
    /// Обработчик сообщения получения всех студентов команды.
    /// </summary>
    public class GetAllStudentsByTeamIdConsumer : IConsumer<GetAllStudentsByTeamIdCommand>
    {
        private readonly IServiceAsync<CompositionTeam> compositionTeamService;
        private readonly ILogger<GetAllStudentsByTeamIdConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetAllStudentsByTeamIdConsumer"/>.
        /// </summary>
        /// <param name="compositionTeamService">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetAllStudentsByTeamIdConsumer(IServiceAsync<CompositionTeam> compositionTeamService, ILogger<GetAllStudentsByTeamIdConsumer> logger)
        {
            this.compositionTeamService = compositionTeamService;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetAllStudentsByTeamIdCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения поиска студентов команды.");

            var studentsList = await this.compositionTeamService.GetAsync(t => t.TeamId == context.Message.Id);

            if (studentsList != null)
            {
                await context.RespondAsync(new GetAllStudentsByTeamIdResponse() { StudentsList = studentsList, Result = ResponseResult.Success });
            }

            await context.RespondAsync(new GetAllStudentsByTeamIdResponse { Result = ResponseResult.NotSuccess });
        }
    }
}