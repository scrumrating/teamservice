﻿using System.Collections;
using System.Collections.Generic;
using TeamService.Core.Entities;

namespace TeamService.Consumers.GetAllStudentsByTeamId
{
    /// <summary>
    /// Ответ на команду получения списка студентов команды.
    /// </summary>
    public class GetAllStudentsByTeamIdResponse
    {
        /// <summary>
        /// Получает или задает список мдентификаторов студентов.
        /// </summary>
        public IEnumerable<CompositionTeam> StudentsList { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}