﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TeamService.Core.Interfaces
{
    /// <summary>
    /// Сервисный объект.
    /// </summary>
    public interface IServiceAsync<TEntity> where TEntity : class
    {
        /// <summary>
        /// Создание записи.
        /// </summary>
        /// <param name="item">Сущность для создания.</param>
        /// <returns>Успешное добавление.</returns>
        Task<bool> CreateAsync(TEntity item);

        /// <summary>
        /// Найти и вернуть сущность по ID.
        /// </summary>
        /// <param name="id">Чиcловой идентификатор записи.</param>
        /// <returns>Найденная сущность или null.</returns>
        Task<TEntity> FindByIdAsync(int id);

        /// <summary>
        /// Получить коллекцию строк.
        /// </summary>
        /// <returns>Выборка данных.</returns>
        Task<IEnumerable<TEntity>> GetAsync();

        /// <summary>
        /// Получить коллекцию строк. (Пагинация)
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Выборка данных.</returns>
        Task<IEnumerable<TEntity>> GetAsync(int pageSize, int pageNumber);

        /// <summary>
        /// Получить коллекцию строк по заданному фильтру.
        /// </summary>
        /// <param name="predicate">Условие по кторому будут браться даныне.</param>
        /// <returns>Выборка данных.</returns>
        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Удалить запись.
        /// </summary>
        /// <param name="id">Идентификатор для удаления.</param>
        Task RemoveAsync(int id);

        /// <summary>
        /// Измменить запись.
        /// </summary>
        /// <param name="item">Обновление записи.</param>
        Task UpdateAsync(TEntity item);
    }
}