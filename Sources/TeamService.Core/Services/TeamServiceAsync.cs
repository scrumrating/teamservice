﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TeamService.Core.DTO;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;


namespace TeamService.Core.Services
{
    /// <summary>
    /// Сервисный объект для работы с Team.
    /// </summary>
    public class TeamServiceAsync : IServiceAsync<Team>
    {
        private readonly IRepositoryAsync<TeamDto> repository;
        private readonly ILogger<TeamServiceAsync> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TeamServiceAsync"/>.
        /// </summary>
        /// <param name="repository">Хранилище данных о студентах.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public TeamServiceAsync(IRepositoryAsync<TeamDto> repository, ILogger<TeamServiceAsync> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <inheritdoc />
        public async Task<bool> CreateAsync(Team item)
        {
            this.logger.LogInformation($"Выполняется создание команды {item}.");

            await this.repository.CreateAsync(new TeamDto()
            {
                Name = item.Name,
                OverallRating = item.OverallRating,
            });
            return true;
        }

        /// <inheritdoc />
        public async Task<Team> FindByIdAsync(int id)
        {
            this.logger.LogInformation($"Выполняется поиск команды с id {id}.");
            var team = await this.repository.FindByIdAsync(id);
            Team resultTeam = null;
            if (team != null)
            {
                resultTeam = new Team(team.Id, team.Name, team.OverallRating);
            }

            return resultTeam;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Team>> GetAsync()
        {
            this.logger.LogInformation($"Выполняется поиск всех команды.");
            var teams = await this.repository.GetAsync();
            return teams.Select(team => new Team(team.Id, team.Name, team.OverallRating));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Team>> GetAsync(int pageSize, int pageNumber)
        {
            this.logger.LogInformation($"Выполняется поиск всех команды с количеством записей {pageSize}, номером страницы {pageNumber}.");
            var teams = await this.repository.GetAsync(pageSize, pageNumber);
            return teams.Select(team => new Team(team.Id, team.Name, team.OverallRating));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Team>> GetAsync(Expression<Func<Team, bool>> predicate)
        {
            this.logger.LogInformation($"Выполняется поиск всех команды с условием {predicate}.");
            var teamsDto = await this.repository.GetAsync();
            var teams = teamsDto.Select(team => new Team(team.Id, team.Name, team.OverallRating));
            return teams.Where(predicate.Compile());
        }

        /// <inheritdoc />
        public async Task RemoveAsync(int id)
        {
            this.logger.LogInformation($"Выполняется удаление команды с Id {id}.");
            await this.repository.RemoveAsync(id);
        }

        /// <inheritdoc />
        public async Task UpdateAsync(Team item)
        {
            this.logger.LogInformation($"Выполняется обновление данных команды {item}.");
            await this.repository.UpdateAsync(new TeamDto()
            {
                Id = item.Id,
                Name = item.Name,
                OverallRating = item.OverallRating,
            });
        }
    }
}