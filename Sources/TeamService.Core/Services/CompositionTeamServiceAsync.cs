﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TeamService.Core.DTO;
using TeamService.Core.Entities;
using TeamService.Core.Interfaces;

namespace TeamService.Core.Services
{
    public class CompositionTeamServiceAsync : IServiceAsync<CompositionTeam>
    {
        private readonly IRepositoryAsync<CompositionTeamDto> repository;
        private readonly ILogger<TeamServiceAsync> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CompositionTeamServiceAsync"/>.
        /// </summary>
        /// <param name="repository">Хранилище данных о студентах.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public CompositionTeamServiceAsync(IRepositoryAsync<CompositionTeamDto> repository, ILogger<TeamServiceAsync> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <inheritdoc />
        public async Task<bool> CreateAsync(CompositionTeam item)
        {
            this.logger.LogInformation($"Выполняется регистрация студента в команду {item}.");

            await this.repository.CreateAsync(new CompositionTeamDto()
            {
                TeamId = item.TeamId,
                StudentId = item.StudentId,
            });
            return true;
        }

        /// <inheritdoc />
        public async Task<CompositionTeam> FindByIdAsync(int id)
        {
            this.logger.LogInformation($"Выполняется поиск записи с id {id}.");
            var team = await this.repository.FindByIdAsync(id);
            CompositionTeam resultTeam = null;
            if (team != null)
            {
                resultTeam = new CompositionTeam(team.Id, team.TeamId, team.StudentId);
            }

            return resultTeam;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<CompositionTeam>> GetAsync()
        {
            this.logger.LogInformation($"Выполняется поиск всех записей.");
            var teams = await this.repository.GetAsync();
            return teams.Select(team => new CompositionTeam(team.Id, team.TeamId, team.StudentId));
        }
        
        /// <inheritdoc />
        public async Task<IEnumerable<CompositionTeam>> GetAsyncaaaaaaa()
        {
            this.logger.LogInformation($"Выполняется поиск всех записей.");
            var teams = await this.repository.GetAsync();
            return teams.Select(team => new CompositionTeam(team.Id, team.TeamId, team.StudentId));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<CompositionTeam>> GetAsync(int pageSize, int pageNumber)
        {
            this.logger.LogInformation($"Выполняется поиск всех записей с количеством записей {pageSize}, номером страницы {pageNumber}.");
            var teams = await this.repository.GetAsync(pageSize, pageNumber);
            return teams.Select(team => new CompositionTeam(team.Id, team.TeamId, team.StudentId));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<CompositionTeam>> GetAsync(Expression<Func<CompositionTeam, bool>> predicate)
        {
            this.logger.LogInformation($"Выполняется поиск всех записей с условием {predicate}.");
            var compositionTeamDtos = await this.repository.GetAsync();
            var compositionTeam = compositionTeamDtos.Select(team => new CompositionTeam(team.Id, team.TeamId, team.StudentId));
            return compositionTeam.Where(predicate.Compile());
        }

        /// <inheritdoc />
        public async Task RemoveAsync(int id)
        {
            this.logger.LogInformation($"Выполняется удаление записи с id {id}.");
            await this.repository.RemoveAsync(id);
        }

        /// <inheritdoc />
        public async Task UpdateAsync(CompositionTeam item)
        {
            this.logger.LogInformation($"Выполняется обновление данных {item}.");
            await this.repository.UpdateAsync(new CompositionTeamDto()
            {
                Id = item.Id,
                TeamId = item.TeamId,
                StudentId = item.StudentId,
            });
        }
    }
}