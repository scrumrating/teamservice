﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace TeamService.Core
{
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository teamRepository;
        private readonly ILogger<TeamService> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TeamService"/>.
        /// </summary>
        /// <param name="teamRepository">Хранилище Team.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public TeamService(ITeamRepository teamRepository, ILogger<TeamService> logger)
        {
            this.teamRepository = teamRepository;
            this.logger = logger;
        }

        /// <inheritdoc/>
        public Team Get(int id)
        {
            this.logger.LogInformation($"Выполняется поиск Team с идентификатором '{id}'.");
            return this.teamRepository.Find(id);
        }

        /// <inheritdoc/>
        public List<Team> Get()
        {
            this.logger.LogInformation($"Выполняется поиск всех Team.");
            return this.teamRepository.Find();
        }
    }
}