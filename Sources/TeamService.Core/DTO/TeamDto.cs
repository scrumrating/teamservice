﻿namespace TeamService.Core.DTO
{
    /// <summary>
    /// Класс модель команды.
    /// </summary>
    public class TeamDto: IdentityDto
    {
        /// <summary>
        /// Название команды.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Средний рейтинг команды.
        /// </summary>
        public double OverallRating { get; set; }
    }
}