﻿namespace TeamService.Core.DTO
{
    /// <summary>
    /// Модель состава команды.
    /// </summary>
    public class CompositionTeamDto : IdentityDto
    {
        /// <summary>
        /// Идентификатор команды.
        /// </summary>
        public int TeamId { get; set; }
        
        /// <summary>
        /// Идентификатор студента.
        /// </summary>
        public int StudentId { get; set; }
    }
}