﻿using System;
using TeamService.Core.DTO;

namespace TeamService.Core.Entities
{
    /// <summary>
    /// Модель состава команды.
    /// </summary>
    public class CompositionTeam
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса &lt;see cref="CompositionTeam"/&gt;.
        /// </summary>
        /// <param name="id">Id записи.</param>
        /// <param name="teamId">Id команды.</param>
        /// <param name="studentId">Id студента.</param>
        public CompositionTeam(int id, int teamId, int studentId)
        {
            Id = id;
            TeamId = teamId;
            StudentId = studentId;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса &lt;see cref="CompositionTeam"/&gt;.
        /// </summary>
        /// <param name="teamId">Id команды.</param>
        /// <param name="studentId">Id студента.</param>
        public CompositionTeam(int teamId, int studentId)
        {
            TeamId = teamId;
            StudentId = studentId;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса &lt;see cref="CompositionTeam"/&gt;.
        /// </summary>
        public CompositionTeam() { }

        /// <summary>
        /// Идентификатор записи.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор команды.
        /// </summary>
        public int TeamId { get; set; }

        /// <summary>
        /// Идентификатор студента.
        /// </summary>
        public int StudentId { get; set; }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            var isEqual = false;
            switch (obj)
            {
                case null:
                    throw new ArgumentException("Не может быть null.");

                case CompositionTeam team:
                {
                    var compareToObj = team;

                    if (compareToObj.Id.CompareTo(this.Id) == 0)
                    {
                        isEqual = true;
                    }

                    break;
                }
            }

            return isEqual;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"Id = {this.Id}, TeamId = {this.TeamId}, StudentId = {this.StudentId}";
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}