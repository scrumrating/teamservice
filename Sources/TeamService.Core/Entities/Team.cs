using System;

namespace TeamService.Core.Entities
{
    /// <summary>
    /// Team.
    /// </summary>
    public class Team
    {
       /// <summary>
       /// �������������� ����� ��������� ������ &lt;see cref="Team"/&gt;.
       /// </summary>
       /// <param name="id">������������� �������.</param>
       /// <param name="name">�������� �������.</param>
       /// <param name="overallRating">������� ���� �������.</param>
        public Team(int id, string name, double overallRating)
        {
            this.Id = id;
            this.Name = name;
            this.OverallRating = overallRating;
        }

       /// <summary>
       /// �������������� ����� ��������� ������ &lt;see cref="Team"/&gt;.
       /// </summary>
       public Team() { }

        /// <summary>
        /// �������� ��� ������ ������������� �������.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// �������� �������.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ������� ������� �������.
        /// </summary>
        public double OverallRating { get; set; }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            var isEqual = false;
            switch (obj)
            {
                case null:
                    throw new ArgumentException("�� ����� ���� null.");

                case Team team:
                {
                    var compareToObj = team;

                    if (compareToObj.Id.CompareTo(this.Id) == 0)
                    {
                        isEqual = true;
                    }

                    break;
                }

                case string name:
                {
                    if (string.Compare(name, this.Name, StringComparison.Ordinal) == 0)
                    {
                        isEqual = true;
                    }

                    break;
                }
            }

            return isEqual;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"Id = {this.Id}, Name = {this.Name}, OverallRating = {this.OverallRating}";
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}