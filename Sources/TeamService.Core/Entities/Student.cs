﻿using System.ComponentModel.DataAnnotations;

namespace TeamService.Core.Entities
{
    public class Student
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Student"/>.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <param name="firstName">Имя.</param>
        /// <param name="middleName">Отчество.</param>
        /// <param name="lastName">Фамилия.</param>
        /// <param name="email">Адрес электронной почты.</param>
        public Student(int id, string firstName, string middleName, string lastName, string email)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.MiddleName = middleName;
            this.LastName = lastName;
            this.Email = email;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Student"/>.
        /// </summary>
        public Student() {}

        /// <summary>
        /// Получает или задает идентификатор.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Получает или задает имя студента.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Получает или задает отчество студента.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Получает или задает фамилию студента.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Получает или задает адрес электронной почты.
        /// </summary>
        [Required]
        public string Email { get; set; }

    }
}