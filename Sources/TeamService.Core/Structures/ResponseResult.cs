﻿namespace TeamService.Core.Structures
{
    /// <summary>
    /// Результат запроса.
    /// </summary>
    public static class ResponseResult
    {
        /// <summary>
        /// Задаёт успешный результат.
        /// </summary>
        public const string Success = "success";

        /// <summary>
        /// Задаёт без успешный результат.
        /// </summary>
        public const string NotSuccess = "not-found";
        
        /// <summary>
        /// Задаёт конфликтный результат.
        /// </summary>
        public const string Conflict = "conflict";
    }
}