﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using TeamService.Core.DTO;
using TeamService.Core.Entities;
using TeamService.Data.Context;

namespace TeamService.Data.DataGenerator
{
    /// <summary>
    /// Класс для генирации данных.
    /// </summary>
    public class RandomGen
    {
        private readonly TeamContext context;
        
        public RandomGen(TeamContext context)
        {
            this.context = context;
            if (!context.Teams.Any())
            {
                DataSeedTeams(10);
            }
            
            if (!context.CompositionTeams.Any())
            {
                DataSeedCompositionTeams(10);
            }
        }
        
        public void DataSeedTeams(int dataCount)
        {
            var team = new List<TeamDto>();

            foreach (var item in CreateFakerTeam().GenerateForever())
            {
                if (dataCount == item.Id)
                {
                    break;
                }

                context.Teams.Add(item);
            }
            context.SaveChanges();
        }
        
        
        public void DataSeedCompositionTeams(int dataCount)
        {
            var compositionTeam = new List<CompositionTeamDto>();

            foreach (var item in CreateCompositionTeam().GenerateForever())
            {
                if (dataCount == item.Id)
                {
                    return;
                }

                context.CompositionTeams.Add(item);
            }

            context.SaveChanges();
        }

        /// <summary>
        /// Генирация команд.
        /// </summary>
        /// <returns></returns>
        private Faker<TeamDto> CreateFakerTeam()
        {
            var id = 1;
            var teamFaker = new Faker<TeamDto>()
                .CustomInstantiator(f => new TeamDto()
                {
                    Id = id++
                })
                .RuleFor(u => u.Name, (f, u) => f.Company.CompanyName())
                .RuleFor(u => u.OverallRating, (f, u) => f.Random.Double(0, 10));

            return teamFaker;
        }
        
        /// <summary>
        /// Генирация команд.
        /// </summary>
        /// <returns></returns>
        private Faker<CompositionTeamDto> CreateCompositionTeam()
        {
            var id = 1;
            var compositionTeamFaker = new Faker<CompositionTeamDto>()
                .CustomInstantiator(f => new CompositionTeamDto()
                {
                    Id = id++,
                })
                .RuleFor(u => u.StudentId, (f, u) => f.Random.Int(0, 10))
                .RuleFor(u => u.TeamId, (f, u) => f.Random.Int(0, 10));

            return compositionTeamFaker;
        }
    }
}