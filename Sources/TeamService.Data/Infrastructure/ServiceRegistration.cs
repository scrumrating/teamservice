﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using TeamService.Core.Interfaces;

namespace TeamService.Data.Infrastructure
{
    /// <summary>
    /// Регистрация сервисов
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class ServiceRegistration
    {
        /// <summary>
        /// Добавляет сервисы сборки в контейнер.
        /// </summary>
        /// <param name="services">Сервисы контейнера.</param>
        /// <returns>Изменённый набор сервисов контейнера.</returns>
        public static IServiceCollection AddData(this IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromExecutingAssembly()
                .AddClasses(classes => classes.AssignableTo(typeof(IRepositoryAsync<>)))
                .AsImplementedInterfaces()
                .WithSingletonLifetime());
            return services;
        }
    }
}