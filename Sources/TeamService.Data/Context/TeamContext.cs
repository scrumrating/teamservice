﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TeamService.Core.DTO;
using TeamService.Data.DataGenerator;

namespace TeamService.Data.Context
{
    /// <summary>
    /// Контекст базы данных Team.
    /// </summary>
    public class TeamContext : DbContext
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TeamContext"/>.
        /// </summary>
        public TeamContext(DbContextOptions<TeamContext> options) : base(options)
        {
            Database.EnsureCreated();
            // if (!this.Teams.Any())
            // {
            //      var gen = new RandomGen(this);
            // }
        }
        
        /// <summary>
        /// Получает или задает содержимое базы данных Team (Команды).
        /// </summary>
        public DbSet<TeamDto> Teams { get; set; }

        /// <summary>
        /// Получает или задает содержимое базы данных Composition Team (Состава команды).
        /// </summary>
        public DbSet<CompositionTeamDto> CompositionTeams { get; set; }
    }
}