﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TeamService.Consumers;
using TeamService.Consumers.AddStudentsToTeam;
using TeamService.Consumers.AddStudentToTeam;
using TeamService.Consumers.Create;
using TeamService.Consumers.Delete;
using TeamService.Consumers.GetAllPagination;
using TeamService.Consumers.GetAllStudentsByTeamId;
using TeamService.Consumers.GetTeamById;
using TeamService.Consumers.GetTeamByName;
using TeamService.Consumers.RemoveStudentFromTeam;
using TeamService.Consumers.Update;
using TeamService.Core.Entities;
using TeamService.Core.Structures;

namespace TeamService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер Team.
    /// </summary>
    [Route("/api/team")]
    public class TeamsController : Controller
    {
        private readonly IRequestClient<AddStudentsToTeamCommand> addStudentsToTeamClient;
        private readonly IRequestClient<AddStudentToTeamCommand> addStudentToTeamClient;
        private readonly IRequestClient<GetAllStudentsByTeamIdCommand> getAllStudentsByTeamIdClient;
        private readonly IRequestClient<CreateCommand> createClient;
        private readonly IRequestClient<DeleteCommand> deleteClient;
        private readonly IRequestClient<GetAllCommand> getAllClient;
        private readonly IRequestClient<GetAllPaginationCommand> getAllPaginationClient;
        private readonly IRequestClient<GetTeamByIdCommand> getTeamByIdClient;
        private readonly IRequestClient<GetTeamByNameCommand> getTeamByNameClient;
        private readonly IRequestClient<RemoveStudentFromTeamCommand> removeStudentFromTeamClient;
        private readonly IRequestClient<UpdateCommand> updateClient;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TeamsController"/>.
        /// </summary>
        /// <param name="addStudentsToTeamClient">Клиент добавления студентов в команду.</param>
        /// <param name="addStudentToTeamClient">Клиент добавления студента в команду.</param>
        /// <param name="getAllStudentsByTeamIdClient">Клиент поиска всех студентов команды.</param>
        /// <param name="createClient">Клиент добавление новой команды.</param>
        /// <param name="deleteClient">Клиент удаления команды.</param>
        /// <param name="getAllClient">Клиент получения всех данных о командых.</param>
        /// <param name="getAllPaginationClient">Клиент получения всех данных о командыхс пагинацией.</param>
        /// <param name="teamByIdClient">Клиент получения данных о комнде по id.</param>
        /// <param name="teamByNameClient">Клиент получения данных о комманде по названиею.</param>
        /// <param name="removeStudentFromTeamClient">Клиент удаления данных студента из команды.</param>
        /// <param name="updateClient">Клиент обновления данных о команде.</param>
        public TeamsController(
            IRequestClient<AddStudentsToTeamCommand> addStudentsToTeamClient,
            IRequestClient<AddStudentToTeamCommand> addStudentToTeamClient,
            IRequestClient<GetAllStudentsByTeamIdCommand> getAllStudentsByTeamIdClient,
            IRequestClient<CreateCommand> createClient,
            IRequestClient<DeleteCommand> deleteClient,
            IRequestClient<GetAllCommand> getAllClient,
            IRequestClient<GetAllPaginationCommand> getAllPaginationClient,
            IRequestClient<GetTeamByIdCommand> teamByIdClient,
            IRequestClient<GetTeamByNameCommand> teamByNameClient,
            IRequestClient<RemoveStudentFromTeamCommand> removeStudentFromTeamClient,
            IRequestClient<UpdateCommand> updateClient)
        {
            this.addStudentsToTeamClient = addStudentsToTeamClient;
            this.addStudentToTeamClient = addStudentToTeamClient;
            this.getAllStudentsByTeamIdClient = getAllStudentsByTeamIdClient;
            this.createClient = createClient;
            this.deleteClient = deleteClient;
            this.getAllClient = getAllClient;
            this.getAllPaginationClient = getAllPaginationClient;
            this.getTeamByIdClient = teamByIdClient;
            this.getTeamByNameClient = teamByNameClient;
            this.removeStudentFromTeamClient = removeStudentFromTeamClient;
            this.updateClient = updateClient;
        }

        /// <summary>
        /// Метод для получения доступным методов.
        /// </summary>
        /// <returns>Список доступных методов.</returns>
        [HttpOptions]
        public IActionResult Options()
        {
            this.Response.Headers.Add("Allow", "GET, OPTIONS");
            return this.Ok();
        }

        /// <summary>
        /// Получить список команд.
        /// </summary>
        /// <returns>Список Team.</returns>
        /// <response code="200">Команды найдены.</response>
        /// <response code="404">Команда не найдена.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var response =
                    await this.getAllClient.GetResponse<GetAllResponse>(new GetAllCommand());

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok(response.Message.Teams);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить список команд. (Пагинация)
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Список Team.</returns>
        /// <response code="200">Список команд (пагинация) найден.</response>
        /// <response code="404">Команды не найдены.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpGet]
        [Route("All")]
        public async Task<IActionResult> Get([FromQuery] int pageSize, [FromQuery] int pageNumber)
        {
            try
            {
                var command = new GetAllPaginationCommand()
                {
                    PageSize = pageSize,
                    PageNumber = pageNumber,
                };

                var response = await this.getAllPaginationClient.GetResponse<GetAllPaginationResponse>(command);

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok(response.Message.Teams);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить список студентов по id команде.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns>Список студентов команды.</returns>
        /// <response code="200">Студенты найдены.</response>
        /// <response code="404">Команда или студенты не найдены.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpGet]
        [Route("GetByTeamId")]
        public async Task<IActionResult> GetByTeamId([FromQuery] int id)
        {
            try
            {
                var response =
                    await this.getAllStudentsByTeamIdClient.GetResponse<GetAllStudentsByTeamIdResponse>(
                        new GetAllStudentsByTeamIdCommand() { Id = id });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok(response.Message.StudentsList.Select(x => x.StudentId));
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить команду по её идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns>Объект найденой команды.</returns>
        /// <response code="200">Команда найдена.</response>
        /// <response code="404">Команда не найдена.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> Get([FromQuery(Name = "id")] int id)
        {
            try
            {
                var response =
                    await this.getTeamByIdClient.GetResponse<GetTeamByIdResponse>(new GetTeamByIdCommand() {Id = id});

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok(response.Message.Team);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить команду по навзанию.
        /// </summary>
        /// <param name="name">Название команды.</param>
        /// <returns>Объект найденной команды.</returns>
        /// <response code="200">Команда найдена.</response>
        /// <response code="404">Команда не найдена.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpGet]
        [Route("GetByName")]
        public async Task<IActionResult> Get([FromQuery(Name = "name")] string name)
        {
            try
            {
                var response =
                    await this.getTeamByNameClient.GetResponse<GetTeamByNameResponse>(new GetTeamByNameCommand()
                        {Name = name});

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok(response.Message.Team);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Создание команды.
        /// </summary>
        /// <param name="team">Объект команды.</param>
        /// <response code="200">Команда создана.</response>
        /// <response code="404">Команда не найдена.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Team team)
        {
            try
            {
                var response =
                    await this.createClient.GetResponse<CreateResponse>(new CreateCommand() {Team = team});

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Добавление студентов в группу.
        /// </summary>
        /// <param name="studenstId">Идентификаторы студентов.</param>
        /// <param name="teamId">Объект команды.</param>
        /// <response code="200">Студенты добавлены в команду.</response>
        /// <response code="404">Команда не найдена.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpPost]
        [Route("AddStudents")]
        public async Task<IActionResult> Post([FromBody] List<int> studenstId, [FromQuery] int teamId)
        {
            try
            {
                var response =
                    await this.addStudentsToTeamClient.GetResponse<AddStudentsToTeamResponse>(
                        message: new AddStudentsToTeamCommand()
                        {
                            StudentsId = studenstId,
                            Team = new Team()
                            {
                                Id = teamId,
                            },
                        });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Добавление студента в группу.
        /// </summary>
        /// <param name="studentId">Идентификатор студента.</param>
        /// <param name="team">Объект команды.</param>
        /// <response code="200">Студент добавлен в команду.</response>
        /// <response code="404">Команда не найдена.</response>
        /// <response code="409">Студент в команде уже существует.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpPost]
        [Route("AddStudent")]
        public async Task<IActionResult> Post([FromQuery(Name = "teamId")] int teamId,
            [FromQuery(Name = "studentId")] int studentId)
        {
            try
            {
                var response =
                    await this.addStudentToTeamClient.GetResponse<AddStudentToTeamResponse>(
                        new AddStudentToTeamCommand()
                        {
                            StudentId = studentId,
                            TeamId = teamId,
                        });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok();
                    case ResponseResult.Conflict:
                        return new StatusCodeResult((int) HttpStatusCode.Conflict);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Обновление данных команды.
        /// </summary>
        /// <param name="team">Объект команды.</param>
        /// <response code="200">Команда обновлена.</response>
        /// <response code="404">Команда не найдена.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Team team)
        {
            try
            {
                var response =
                    await this.updateClient.GetResponse<UpdateResponse>(new UpdateCommand() {Team = team});

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Удаление студента из группы.
        /// </summary>
        /// <param name="teamId">Объект команды.</param>
        /// <param name="studentId">Идентификатор студента.</param>
        /// <response code="200">Студент удалён из команды.</response>
        /// <response code="404">Студент в команде не найдена.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpDelete]
        [Route("RemoveStudent")]
        public async Task<IActionResult> Delete([FromQuery(Name = "teamId")] int teamId,
            [FromQuery(Name = "studentId")] int studentId)
        {
            try
            {
                var response =
                    await this.removeStudentFromTeamClient.GetResponse<RemoveStudentFromTeamResponse>(
                        new RemoveStudentFromTeamCommand()
                        {
                            StudentId = studentId,
                            TeamId = teamId,
                        });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Удаление команды.
        /// </summary>
        /// <param name="id">Объект для удаления.</param>
        /// <response code="200">Команда удалена.</response>
        /// <response code="404">Команда не найдена.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery(Name = "id")] int id)
        {
            try
            {
                var response = await this.deleteClient.GetResponse<DeleteResponse>(
                    new DeleteCommand()
                    {
                        Id = id,
                    });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int) HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int) HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int) HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int) HttpStatusCode.BadGateway);
            }
        }
    }
}