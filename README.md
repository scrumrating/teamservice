# TeamService

Веб сервис для работы с сущностью "Команды"

## Построение сервиса
Для построения сервиса необходимо запустить скрип построения командой

```
./build.ps1
```

## Запуск сервиса

В процессе построения в папке ```./artifacts/packages``` будет создан NuGet пакет с исполняемыми файлами сервиса.

Для запуска сервиса необходимо извлечь содержимое NuGet пакета и выполнить команду

```
dotnet lib/netcoreapp2.2/TeamService.Instance.dll
```

или запустить исполняемые файлы, созданные в процессе посторения командой

```
dotnet sources/TeamService.Instance/release/netcoreapp2.2/TeamService.Instance.dll
```

После успешного запуска сервиса должнен быть доступен следующий метод: 

- ```GET localhost:5000/health``` 

В случае успешного запуска сервиса будет получен ответ ```200 - OK```. 


## Поднятие docker-compose

1. Открыть папку TeamService.docker в терминале/cmd. 
2. Запустить команду
```
docker-compose up -d --build
```
3. docker-compose поднимет три контейнера: **RabbitMQ**, **PostgresSql**, **.NetCore (Api)**
    Доступные по следующим ссылкам:

    RabbitMQ - <http://127.0.0.1:15672> (login: guest, password: guest)

    .NetCore (Api) Swagger - <http://127.0.0.1:555/swagger/index.html>

    Соответственно все доступные API будут на порту 555. При желании можно поменять порт на другой в файле docker-composeюньд в строчке 
```
ports:
      - 555:80
```

### Остоновить docker-compose

1. Останваливаем docker-compose командой
```
docker-compose stop
```

2. Ещё чтобы нверняка можно удалить остановленные контейнеры командой
```
docker rm $(docker ps -a -f status=exited -q)
```