using FakeItEasy;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System.Collections.Generic;

namespace TeamService.Core.Tests
{
    public class TeamServiceTests
    {
        // [Test]
        // [Description("Когда есть Team возвращается список Team.")]
        // public void CanGetAllTeams()
        // {
        //     List<Team> Teams = new List<Team> { new Team(1), new Team(2) };
        //     ITeamRepository repository = A.Fake<ITeamRepository>();
        //     A.CallTo(() => repository.Find()).Returns(Teams);
        //     ILogger<GenericService> logger = A.Fake<ILogger<GenericService>>();
        //
        //     GenericService sut = new GenericService(repository, logger);
        //
        //     List<Team> result = sut.Get();
        //     result.Should().NotContainNulls();
        //     result.Should().ContainEquivalentOf(new Team(1));
        //     result.Should().ContainEquivalentOf(new Team(2));
        // }
        //
        // [Test]
        // [Description("Когда есть Team возвращается Team.")]
        // public void CanTeam()
        // {
        //     Team team = new Team(1);
        //     ITeamRepository repository = A.Fake<ITeamRepository>();
        //     A.CallTo(() => repository.Find(A<int>._)).Returns(team);
        //     ILogger<GenericService> logger = A.Fake<ILogger<GenericService>>();
        //
        //     GenericService sut = new GenericService(repository, logger);
        //
        //     Team result = sut.Get(123);
        //     result.Should().Be(team);
        // }
        //
        // [Test]
        // [Description("Когда нет Team возвращается null.")]
        // public void CantGetInexistingTeam()
        // {
        //     ITeamRepository repository = A.Fake<ITeamRepository>();
        //     A.CallTo(() => repository.Find(A<int>._)).Returns(null);
        //     ILogger<GenericService> logger = A.Fake<ILogger<GenericService>>();
        //
        //     GenericService sut = new GenericService(repository, logger);
        //
        //     Team result = sut.Get(123);
        //     result.Should().Be(null);
        // }
    }
}